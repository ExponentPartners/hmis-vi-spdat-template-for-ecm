﻿import os
import subprocess
import json
from collections import namedtuple
from ..util.command import runCommand, runCommandJSON




def does_org_alias_exist(orgAlias):
    orgRetList = org_list()
    print('Checking if org '+orgAlias+' exists')
    for i in orgRetList.result.scratchOrgs:
        org = i
        print(org)
        if (hasattr(org, 'alias') and org.alias == orgAlias):
            return True

    return False

def org_list():
    command = 'sfdx force:org:list'
    os.system(command)
    return runCommandJSON(command + ' --json')

def org_display(orgAlias, constructLoginUrl):
    command = 'sfdx force:org:display '
    command += '-u ' + orgAlias
    os.system(command)
    ret = runCommandJSON(command + ' --json')

    if (constructLoginUrl == True):
        print(ret.result.instanceUrl + 'secur/frontdoor.jsp?sid=' + ret.result.accessToken)

    return ret

def org_create(scratchOrgAlias, setDefault, devhubOrg):
    command = 'sfdx force:org:create -v ' + devhubOrg + ' -f config/project-scratch-def.json  -w 30 '
    command += ' --setalias ' + scratchOrgAlias
    command += ' --setdefaultusername' if setDefault else ''
    return runCommandJSON(command + ' --json')

def org_delete(scratchOrgAliasToDelete, targetdevhubusername):
    command = 'sfdx force:org:delete -p -u ' + scratchOrgAliasToDelete + ' -v ' + targetdevhubusername
    return runCommandJSON(command + ' --json')
