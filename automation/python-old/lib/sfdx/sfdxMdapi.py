import os
import subprocess
import json
from collections import namedtuple

def mdapi_deploy(filepath, scratchOrgAlias):
    command = 'sfdx force:mdapi:deploy -d "'+filepath+'" -w 30 -u '+scratchOrgAlias
    print(command)
    # os.system(command)
    ret = json.loads(subprocess.check_output(command + ' --json', shell=True), object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))
    print(ret)
    return ret





