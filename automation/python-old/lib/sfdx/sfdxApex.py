
import os
import subprocess
import json
from ..color.bcolors import bcolors
from collections import namedtuple
import glob
from ..util.command import *

def execute(scriptname, orgAlias):
    command = 'sfdx force:apex:execute -f '+scriptname + ' -u ' + orgAlias
    print(command)
    ret = runCommand(command + ' --json')
    print(print(json.dumps(json.loads(ret), indent=2)))
    jsonRet = json.loads(ret, object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))
    return jsonRet

### sort eth scripts based on the number preface
def execute_scripts_in_folder(folder, orgAlias):
    apexFileList = glob.glob(folder + "*.apex")
    apexFileList.sort(key=lambda val: getPlaceOfScript(val))


    for apexFile in apexFileList:
        execute(apexFile, orgAlias)

    return

def getPlaceOfScript(fullpath):
    print(fullpath)
    pathList = fullpath.split('\\')
    placeStr = pathList[len(pathList)-1].split('_')[0]
    place = int(placeStr)
    print(place)
    return place

def runAllTests():
    print(bcolors.WARNING + 'Running All Apex Tests. This may take several minutes . . . .')
    command = 'sfdx force:apex:test:run -w 30 -c -r human -y --json'
    retStr = runCommand(command)
    ret = json.loads(retStr)
    print('Results of Apex Tests:')

    if (ret['result']['summary']['outcome'] != "Passed"):
        raise Exception("All Apex Unit Tests Did Not pass")

    # EXAMPLE SUMMARY OBJECT
    # "summary":{
    #     "outcome":"Passed",
    #     "testsRan":12,
    #     "passing":12,
    #     "failing":0,
    #     "skipped":0,
    #     "passRate":"100%",
    #     "failRate":"0%",
    #     "testStartTime":"Oct 14, 2020 10:30 AM",
    #     "testExecutionTime":"17113 ms",
    #     "testTotalTime":"17113 ms",
    #     "commandTime":"26304 ms",
    #     "hostname":"https://energy-ruby-243-dev-ed.cs18.my.salesforce.com",
    #     "orgId":"00D1100000C5BU5EAN",
    #     "username":"test-rhwaaet5ec5f@example.com",
    #     "testRunId":"70711000020Gwxd",
    #     "userId":"005110000094uDUAAY",
    #     "testRunCoverage":"64%",
    #     "orgWideCoverage":"64%"
    # },

    print(bcolors.WARNING + 'Test Outcome: ' + ret['result']['summary']['outcome'])
    print(bcolors.WARNING + 'Code Coverage: ' + ret['result']['summary']['orgWideCoverage'])
    print(ret['result']['summary'])

    return ret


