import os
import subprocess
import json
from collections import namedtuple
from sfdx.sfdxAuth import auth_web_login
from sfdx.sfdxSource import source_push
from sfdx.sfdxMdapi import *
from sfdx.sfdxPackage import *
from sfdx.sfdxOrgs import *
from sfdx.sfdxApex import *
from sfdx.sfdxUser import *

# Change the current working directory so the sfdx cli can find force-app
os.chdir('..\..')
print("Current Working Directory ", os.getcwd())

# Standard Final vars
QaOrgAlias = 'ExpECM_HMIS-QA-scratch-org'
packageName = 'ExpECM_HMIS'
devHubAlias = 'DeveloperDevHub'
deleteQaOrg = True
setNewOrgAsDefault = False

# determine if we should delete the existing org
if (deleteQaOrg and does_org_alias_exist(QaOrgAlias)):
    org_delete(QaOrgAlias, devHubAlias)

# If or is found, don't create a new one
if (does_org_alias_exist(QaOrgAlias) == False):
    org_create(QaOrgAlias, setNewOrgAsDefault, devHubAlias)

# print out Org List
org_list()

# Install ECM v14.36
sfdxPackageEcmDependency = getPackageDependency()
package_install(sfdxPackageEcmDependency, QaOrgAlias)

# Grab the latest HMIS package and Install it onto the QA Scratch Org
letestVersionAlias = get_latest_version_alias(packageName)
package_install(letestVersionAlias, QaOrgAlias)

# Deploy the template that configures ECM onto the scratch org
mdapi_deploy('./automation/EcmConfiguration/QaConfiguration/src', QaOrgAlias)
execute_scripts_in_folder('.\\automation\\EcmConfiguration\\QaConfiguration\\apex\\', QaOrgAlias)

# Spit out the information on the scratch org use for QA, including an auto login link
org_display(QaOrgAlias, True)


generatePassword(QaOrgAlias, devHubAlias, QaOrgAlias)
get_latest_package_install_url(packageName)
displayUserInfo(devHubAlias, QaOrgAlias)
