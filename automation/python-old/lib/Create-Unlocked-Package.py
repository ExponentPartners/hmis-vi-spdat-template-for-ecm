import os
import subprocess
import json
from collections import namedtuple

from sfdx.sfdxOrgs import org_list
from sfdx.sfdxPackage import *
from sfdx.sfdxApex import *

curPackageName = 'Unlocked Test'
DevHubOrg = 'DeveloperDevHub'

# Change the current working directory so the sfdx cli can find force-app
os.chdir('..')
os.chdir('..')
print("Current Working Directory ", os.getcwd())

sfdx_unlocked_package_version_create(curPackageName, DevHubOrg)
get_latest_package_install_url(curPackageName, DevHubOrg)
