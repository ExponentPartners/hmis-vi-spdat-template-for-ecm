import os
import subprocess
import json
from collections import namedtuple
from sfdx.sfdxAuth import auth_web_login
from sfdx.sfdxSource import source_push
from sfdx.sfdxMdapi import *
from sfdx.sfdxPackage import *
from sfdx.sfdxOrgs import *
from sfdx.sfdxApex import *
from sfdx.sfdxUser import *

# Change the current working directory so the sfdx cli can find force-app
os.chdir('..\..')
print("Current Working Directory ", os.getcwd())


# Standard Final vars
QaOrgAlias = 'ExpECM_HMIS-QA-scratch-org'
QaUserJson = 'qa-user-def.json'
devHubAlias = 'DeveloperDevHub'
packageName = 'ExpECM_HMIS'
deleteQaOrg = True
setNewOrgAsDefault = False



# createUser(QaUserJson, QaOrgAlias)
generatePassword(QaOrgAlias, devHubAlias, QaOrgAlias)
displayUserInfo(devHubAlias, QaOrgAlias)