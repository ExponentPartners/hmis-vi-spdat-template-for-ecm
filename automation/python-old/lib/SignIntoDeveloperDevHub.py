import os
import subprocess
import json
from collections import namedtuple
from sfdx.sfdxAuth import *
from sfdx.sfdxSource import *
from sfdx.sfdxMdapi import *
from sfdx.sfdxPackage import *
from sfdx.sfdxOrgs import *
from sfdx.sfdxApex import *

# Change the current working directory so the sfdx cli can find force-app
os.chdir('..\..')
print("Current Working Directory ", os.getcwd())


newScratchOrgAliasName = 'cur-ExpECM_HMIS-scratch-org'
DeveloperDevHubAlias = 'DeveloperDevHub'


auth_web_login(DeveloperDevHubAlias, 'https://login.salesforce.com/')