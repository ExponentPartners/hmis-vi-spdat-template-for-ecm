import os
from lib.sfdx.sfdxSource import *
from lib.util.arguments import *
from lib.sfdx.sfdxPackage import *

# Change the current working directory so the sfdx cli can find force-app
os.chdir('..')
os.chdir('..')
print("Current Working Directory ", os.getcwd())


curPackageName = 'Unlocked Test'
DevHubOrg = 'ChrisDeveloperDevHub'

get_latest_package_install_url(curPackageName, DevHubOrg)