This folder holds all the info to initialize ECM with test data. Each configuration will have it's own folder name in ```\automation\EcmConfiguration```. Only one of these configurations should be loaded into an org at a time. If one configuration need to be reliant on another configurations, please dupliucate that configuration and add the additional items needed, instead of referencing a previous configuration.


## Apex Script Setup ##

The apex scripts will be executed in order. Please preface the scripts with a number. The Scripts are sorted by alphabetical order. Uninstall scripts are held in their own folder