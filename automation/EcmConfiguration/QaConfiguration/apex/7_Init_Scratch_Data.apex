/**
 * This is an anonymous Apex script that create configuration and component
 * setting records for the facesheet PDF of the template.
 *
 * Any exceptions are caught and re-thrown, after rolling back all operations
 */

Savepoint sp = Database.setSavepoint();
try {
  Map<String,Schema.RecordTypeInfo> rts = SObjectType.ExpECM__ECM_Configuration__c.getRecordTypeInfosByName();
  ExpECM__ECM_Configuration__c config = new ExpECM__ECM_Configuration__c(
    Name =                            'Starter Program PDF Face Sheet Configuration (ECMT)',
    ExpECM__Page_Prefix__c =          'ecmtSPPDFFaceSheet',                       // unique
    ExpECM__Maximum_Page_Count__c =   1,
    ExpECM__Non_Visual_Field_Set__c = 'ExpECM__ECM_Non_Visual_Field_Set',
    ExpECM__PDF_Rendering__c =        true,
    RecordTypeId =                    rts.get('Intake').getRecordTypeId()
  );
  insert config;

  ExpECM__Program__c program = [SELECT Id FROM ExpECM__Program__c WHERE ExpECM__Program_Short_Name__c = 'ECMTSP'];
  program.ExpECM__Default_Face_Sheet_Configuration__c = config.Id;
  update program;

  rts = SObjectType.ExpECM__Component_Settings__c.getRecordTypeInfosByName();
  ExpECM__Component_Settings__c[] settings = new ExpECM__Component_Settings__c[] {
    new ExpECM__Component_Settings__c(
      ExpECM__ECM_Configuration__c =  config.Id,
      ExpECM__Label__c =              'Case Record Summary (Starter Program ECMT)',      // unique
      ExpECM__Page_Number__c =        '1',
      ExpECM__Order__c =              1,
      ExpECM__Section_Title__c =      'Case Record Summary',
      ExpECM__Field_Set_Name__c =     'ExpECM_HMIS__ecmtFacesheetSummary',
      RecordTypeId =                  rts.get('Case Record').getRecordTypeId()
    ),
    new ExpECM__Component_Settings__c(
      ExpECM__ECM_Configuration__c =  config.Id,
      ExpECM__Label__c =              'PDF Client Name and Contact Information (Starter Program ECMT)',         // unique
      ExpECM__Page_Number__c =        '1',
      ExpECM__Order__c =              2,
      ExpECM__Section_Title__c =      'Client Name and Contact Information',
      ExpECM__Field_Set_Name__c =     'ExpECM_HMIS__ecmtClientNameAndContactInfo',
      RecordTypeId =                  rts.get('Contact').getRecordTypeId()
    ),
    new ExpECM__Component_Settings__c(
      ExpECM__ECM_Configuration__c =  config.Id,
      ExpECM__Label__c =              'PDF Referral Information (Starter Program ECMT)',          // unique
      ExpECM__Page_Number__c =        '1',
      ExpECM__Order__c =              3,
      ExpECM__Section_Title__c =      'Referral Information',
      ExpECM__Field_Set_Name__c =     'ExpECM_HMIS__ecmtReferralInformation',
      RecordTypeId =                  rts.get('Case Record').getRecordTypeId()
    ),
    new ExpECM__Component_Settings__c(
      ExpECM__ECM_Configuration__c =  config.Id,
      ExpECM__Label__c =              'PDF Screening and Eligibility (Starter Program ECMT)',               // unique
      ExpECM__Page_Number__c =        '1',
      ExpECM__Order__c =              4,
      ExpECM__Section_Title__c =      'Status',
      ExpECM__Field_Set_Name__c =     'ExpECM_HMIS__ecmtScreeningAndEligibility',
      RecordTypeId =                  rts.get('Case Record').getRecordTypeId()
    ),
    new ExpECM__Component_Settings__c(
      ExpECM__ECM_Configuration__c =  config.Id,
      ExpECM__Label__c =              'PDF Client Demographics (Starter Program ECMT)',     // unique
      ExpECM__Page_Number__c =        '1',
      ExpECM__Order__c =              5,
      ExpECM__Section_Title__c =      'Client Demographics',
      ExpECM__Field_Set_Name__c =     'ExpECM_HMIS__ecmtClientDemographics',
      RecordTypeId =                  rts.get('Contact').getRecordTypeId()
    ),
    new ExpECM__Component_Settings__c(
      ExpECM__ECM_Configuration__c =  config.Id,
      ExpECM__Label__c =              'PDF Household Demographics (Starter Program ECMT)',              // unique
      ExpECM__Page_Number__c =        '1',
      ExpECM__Order__c =              6,
      ExpECM__Section_Title__c =      'Household Demographics',
      ExpECM__Field_Set_Name__c =     'ExpECM_HMIS__ecmtHouseholdDemographics',
      RecordTypeId =                  rts.get('Client Household').getRecordTypeId()
    ),
    new ExpECM__Component_Settings__c(
      ExpECM__ECM_Configuration__c =   config.Id,
      ExpECM__Label__c =               'PDF Household Members (Starter Program ECMT)',        // unique
      ExpECM__Page_Number__c =         '1',
      ExpECM__Order__c =               7,
      ExpECM__Section_Title__c =       'Household Members',
      ExpECM__Field_Set_Name__c =      'ExpECM_HMIS__ecmtHouseholdMembers',
      RecordTypeId =                  rts.get('Household Members').getRecordTypeId()
    ),
    new ExpECM__Component_Settings__c(
      ExpECM__ECM_Configuration__c =  config.Id,
      ExpECM__Label__c =              'PDF Emergency Contact Information (Starter Program ECMT)',             // unique
      ExpECM__Page_Number__c =        '1',
      ExpECM__Order__c =              8,
      ExpECM__Section_Title__c =      'Emergency Contact Information',
      ExpECM__Field_Set_Name__c =     'ExpECM_HMIS__ecmtEmergencyContact',
      RecordTypeId =                  rts.get('Contact').getRecordTypeId()
    ),
    new ExpECM__Component_Settings__c(
      ExpECM__ECM_Configuration__c =  config.Id,
      ExpECM__Label__c =              'PDF Education History (Starter Program ECMT)',        // unique
      ExpECM__Page_Number__c =        '1',
      ExpECM__Order__c =              9,
      ExpECM__Section_Title__c =      'Education History',
      ExpECM__Field_Set_Name__c =     'ExpECM_HMIS__ecmtEducationHistory',
      ExpECM__SObject__c =            'ExpECM__Education_History__c',
      ExpECM__Relates_to_Objects__c = 'Contact',
      ExpECM__Contact_Field_Name__c = 'ExpECM__Client__c',
      RecordTypeId =                  rts.get('Child Object List').getRecordTypeId()
    ),
    new ExpECM__Component_Settings__c(
      ExpECM__ECM_Configuration__c =  config.Id,
      ExpECM__Label__c =              'PDF Baseline Assessment (Starter Program ECMT)',        // unique
      ExpECM__Page_Number__c =        '1',
      ExpECM__Order__c =              10,
      ExpECM__Section_Title__c =      'Baseline Assessment',
      ExpECM__Field_Set_Name__c =     'ExpECM_HMIS__ecmtBaselineAssessment',
      ExpECM__SObject__c =            'ExpECM__Assessment__c',
      ExpECM__Relates_to_Objects__c = 'Case_Record__c',
      ExpECM__Case_Record_Field_Name__c = 'ExpECM__Case_Record__c',
      RecordTypeId =                  rts.get('Child Object List').getRecordTypeId()
    )
  };
  insert settings;
} catch (Exception ex) {
  Database.rollback(sp);
  throw ex;
}
