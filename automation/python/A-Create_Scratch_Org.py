import os
from lib.modules.Pipeline.common.Arguments import ArgumentsDef
from lib.modules.Pipeline.common.org import sfdxOrg

# Change the current working directory so the sfdx cli can find force-app
os.chdir('..')
os.chdir('..')
print("Current Working Directory ", os.getcwd())


ScratchOrgAlias, DevHubOrgAlias = ArgumentsDef.readArguments()

newScratchOrgAliasName = ScratchOrgAlias
devHubAlias = DevHubOrgAlias

# devHubAlias = '__PBOExpDevHub__'
# newScratchOrgAliasName = 'test-hmis-accelerator-scratch-org-1'
# devHubAlias = 'DeveloperDevHub'
# # devHubAlias = '__PBOExpDevHub__'


if (newScratchOrgAliasName == ''):
    raise Exception('newScratchOrgAliasName must be supplied')
if (DevHubOrgAlias == ''):
    raise Exception('DevHubOrgAlias must be supplied')

sfdxOrg.org_create(newScratchOrgAliasName, True, devHubAlias)