import os
import subprocess
import json
from collections import namedtuple
from sfdx.sfdxAuth import *
from sfdx.sfdxSource import *
from sfdx.sfdxMdapi import *
from sfdx.sfdxPackage import *
from sfdx.sfdxOrgs import *
from sfdx.sfdxApex import *
from sfdx.sfdxUser import *
from util.command import *
from util.arguments import *

# Change the current working directory so the sfdx cli can find force-app
print("Current Working Directory ", os.getcwd())
initialWorkingDirector = os.getcwd()


print("--------Running Steps-----------")

os.chdir(initialWorkingDirector)
exec(open("STEP-DisplayAncestorTree.py").read())

