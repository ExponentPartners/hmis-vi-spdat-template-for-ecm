import subprocess
import os
import json

from collections import namedtuple
from color.bcolors import bcolors


# userBashPrint = True
#
# def setBashPrint(bool):
#     userBashPrint = bool
#
#
# def bashPrintout(str):
#
#     print(userBashPrint)
#
#     if userBashPrint:
#         str = "echo \"" + str + "\""
#         os.system(str)
#     else:
#         print(str)

def displayHumanReadableCommand(command):
    os.system(command)

def runCommand(command):
    try:
        print(bcolors.OKBLUE + 'Executing command: ' + command)
        return subprocess.check_output(command,shell=True,stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError as e:
        print
        raise RuntimeError("command '{}' return with error (code {}): {}".format(e.cmd, e.returncode, e.output))

def runCommandJSON(command):

    # Add --json if not supplied. We are converting to JSON so this is needed
    if (command.find('--json') != -1):
        command = command
    else :
        command = command + ' --json'

    # convert to JSON object (as opposed to dictionary)
    commandReturnStr = runCommand(command)
    ret = json.loads(commandReturnStr, object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))
    print(bcolors.OKGREEN + json.dumps(json.loads(commandReturnStr), indent=2))
    return ret

def jsonToObject(str):
    return json.loads(str, object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))

def getJsonFromFile(filepath):
    with open(filepath) as json_file:
        data = json.load(json_file)

    return data

def getPackageDependency():
    x = getJsonFromFile('sfdx-project.json')
    return x['packageDirectories'][0]['dependencies'][0]['package']

def getPackageDependencies():

    filename = 'sfdx-project.json'

    print('getPackageDependencies Path : ' + os.getcwd())

    if '/automation' in os.getcwd():
        filename = '../' + filename
    if '\\automation' in os.getcwd():
        filename = '../' + filename

    print('getPackageDependencies filename : ' + filename)
    x = getJsonFromFile(filename)
    ret = []
    for x in x['packageDirectories'][0]['dependencies']:
        ret.append(x['package'])

    print("found Dependencies")
    for x in ret:
        print(x)

    return ret