﻿import os
import subprocess
import json
from collections import namedtuple
import sys, getopt


def readArguments():
    print('Start reading arguments')

    ScratchOrgAlias = ''
    DevHubOrgAlias = ''

    nextArg = None
    for arg in sys.argv:
        # print('-------------------')
        # print('arg ->' + arg)
        if nextArg == None:
            if arg == '--ScratchOrgAlias':
                nextArg = arg
            if arg == '--DevHubOrgAlias':
                nextArg = arg
        elif nextArg != None:
            if nextArg == '--ScratchOrgAlias':
                ScratchOrgAlias = arg
            if nextArg == '--DevHubOrgAlias':
                DevHubOrgAlias = arg
            nextArg = None

        # print('nextArg ->' + str(nextArg))

    print('-------------Found Arguments-----------------------------------------------')
    print('Number of arguments:' + str(len(sys.argv)) + ' arguments.')
    print('Argument List:' + str(sys.argv))
    print('-------------Parsed Arguments----------------------------------------------')
    print('ScratchOrgAlias -> ' + ScratchOrgAlias)
    print('DevHubOrgAlias -> ' + DevHubOrgAlias)

    return ScratchOrgAlias, DevHubOrgAlias