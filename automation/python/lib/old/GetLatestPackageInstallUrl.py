import os
import subprocess
import json
from collections import namedtuple

from sfdx.sfdxOrgs import org_list
from sfdx.sfdxPackage import *

curPackageName = 'ExpECM_HMIS'

# Change the current working directory so the sfdx cli can find force-app
os.chdir('..')
print("Current Working Directory " , os.getcwd())

get_latest_package_install_url(curPackageName)
