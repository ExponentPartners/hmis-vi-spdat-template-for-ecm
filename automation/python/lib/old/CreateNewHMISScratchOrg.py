import os
import subprocess
import json
from collections import namedtuple
from sfdx.sfdxAuth import *
from sfdx.sfdxSource import *
from sfdx.sfdxMdapi import *
from sfdx.sfdxPackage import *
from sfdx.sfdxOrgs import *
from sfdx.sfdxApex import *
from sfdx.sfdxUser import *
from util.command import *
from util.arguments import *

# Change the current working directory so the sfdx cli can find force-app
os.chdir('..')
os.chdir('..')
os.chdir('..')
print("Current Working Directory ", os.getcwd())


ScratchOrgAlias, DevHubOrgAlias = readArguments()

newScratchOrgAliasName = ScratchOrgAlias
devHubAlias = DevHubOrgAlias

# devHubAlias = '__PBOExpDevHub__'
# newScratchOrgAliasName = 'test-hmis-accelerator-scratch-org-1'
# devHubAlias = 'DeveloperDevHub'
# # devHubAlias = '__PBOExpDevHub__'


if (newScratchOrgAliasName == ''):
    raise Exception('newScratchOrgAliasName must be supplied')
if (DevHubOrgAlias == ''):
    raise Exception('DevHubOrgAlias must be supplied')


sfdxPackageEcmDependencies = getPackageDependencies()


# clean up existing org
if (does_org_alias_exist(newScratchOrgAliasName) == True):
    org_delete(newScratchOrgAliasName, devHubAlias)

org_create(newScratchOrgAliasName, True, devHubAlias)
org_list()
for iPackageDependency in sfdxPackageEcmDependencies:
    package_install(iPackageDependency, newScratchOrgAliasName)

source_push(newScratchOrgAliasName)
# execute_scripts_in_folder('.\\automation\\EcmConfiguration\\QaConfiguration\\apex\\', newScratchOrgAliasName)

generatePassword(newScratchOrgAliasName, devHubAlias, newScratchOrgAliasName)
displayUserInfo(devHubAlias, newScratchOrgAliasName)