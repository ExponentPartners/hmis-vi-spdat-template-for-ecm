import os
from sfdx.sfdxPackage import *
from util.arguments import *

# Change the current working directory so the sfdx cli can find force-app
os.chdir('..')
os.chdir('..')
os.chdir('..')
print("Current Working Directory ", os.getcwd())


ScratchOrgAlias, DevHubOrgAlias = readArguments()

newScratchOrgAliasName = ScratchOrgAlias
devHubAlias = DevHubOrgAlias

# devHubAlias = '__PBOExpDevHub__'
# newScratchOrgAliasName = 'test-hmis-accelerator-scratch-org-1'
# devHubAlias = 'DeveloperDevHub'
# # devHubAlias = '__PBOExpDevHub__'


if (DevHubOrgAlias == ''):
    raise Exception('DevHubOrgAlias must be supplied')

print ('inside STEP-DisplayAncestorTree.py')
packageId = "0Ho4u000000KyjfCAC"
sfdx_package_ancestry_tree(packageId, DevHubOrgAlias)