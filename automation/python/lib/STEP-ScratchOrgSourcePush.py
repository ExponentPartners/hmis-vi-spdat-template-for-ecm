import os
from sfdx.sfdxSource import *
from util.arguments import *

# Change the current working directory so the sfdx cli can find force-app
os.chdir('..')
os.chdir('..')
print("Current Working Directory ", os.getcwd())


ScratchOrgAlias, DevHubOrgAlias = readArguments()

newScratchOrgAliasName = ScratchOrgAlias
devHubAlias = DevHubOrgAlias

# devHubAlias = '__PBOExpDevHub__'
# newScratchOrgAliasName = 'test-hmis-accelerator-scratch-org-1'
# devHubAlias = 'DeveloperDevHub'
# # devHubAlias = '__PBOExpDevHub__'


if (newScratchOrgAliasName == ''):
    raise Exception('newScratchOrgAliasName must be supplied')
if (DevHubOrgAlias == ''):
    raise Exception('DevHubOrgAlias must be supplied')

print ('inside STEP-ScratchOrgSourcePush.py')
source_push(newScratchOrgAliasName)