
import os
import subprocess
import json
from collections import namedtuple
import glob
from util.command import *

def createUser(fileName, orgAlias):
    command = 'sfdx force:user:create -u '+orgAlias+' -f config/'+fileName+' generatePassword=true'
    jsonRet = runCommandJSON(command + ' --json')
    return jsonRet


def displayUserInfo(devHubAlias, userName):
    command = 'sfdx force:user:display ' + ' -v ' + devHubAlias + ' --targetusername ' + userName
    jsonRet = runCommandJSON(command)
    displayHumanReadableCommand(command)
    return jsonRet



def generatePassword(scratchOrgAlias, devHubAlias, user):
    command = 'sfdx force:user:password:generate -u ' + scratchOrgAlias + ' -v ' + devHubAlias + ' -o ' + user
    displayHumanReadableCommand(command)
    jsonRet = runCommandJSON(command)
    print(bcolors.OKGREEN + '------ New User Info Below ------')
    print(bcolors.OKGREEN + 'User Name: ' + user)
    print(bcolors.OKGREEN + 'Password: ' + jsonRet.result.password)
    return jsonRet


