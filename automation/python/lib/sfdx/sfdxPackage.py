﻿import os
import subprocess
import json

from color.bcolors import bcolors
from collections import namedtuple
from util.command import *

def package_install(PackageNameOrId, orgAlias):
    print(bcolors.WARNING + 'This command may take several minutes:')
    command = 'sfdx force:package:install -p "'+PackageNameOrId+'" -w 30 -u ' + orgAlias
    ret = runCommandJSON(command)
    print('Package Install Output:')
    print(ret)
    return ret


def version_list(packageName):
    command = 'sfdx force:package:version:list -v __PBOExpDevHub__ --packages="'+packageName+'"'
    os.system(command)
    return runCommandJSON(command)

def get_latest_package_install_url(packageName, DevHubOrg):
    subscriberId = get_latest_version_subscriber_Id(packageName, DevHubOrg)

    if subscriberId is None:
        print('No Subscriber Id Found')
        return None
    else:
        installUrl = 'https://login.salesforce.com//packagingSetupUI/ipLanding.app?apvId=' + subscriberId
        print('Latest Install Url:')
        print(installUrl)
        return installUrl


def get_latest_package_json(packageName, DevHubOrg):
    command = 'sfdx force:package:version:list -v '  + DevHubOrg + ' -p "'+packageName + '"'
    ret = runCommandJSON(command)

    curLatest = None
    curLatestPackageJson = None
    for packageVersion in ret.result:

        # convert the version number to an integer for easy comparison
        versionToNumber = 0
        MajorVersion = int(packageVersion.MajorVersion) * (10 ** 11)
        MinorVersion = int(packageVersion.MinorVersion) * (10 ** 9)
        PatchVersion = int(packageVersion.PatchVersion) * (10 ** 6)
        BuildNumber = int(packageVersion.BuildNumber) * (10 ** 3)

        versionToNumber = MajorVersion + MinorVersion + PatchVersion + BuildNumber


        if (curLatest == None or curLatest < versionToNumber):
            curLatest = versionToNumber
            curLatestPackageJson = packageVersion


    os.system(command)
    if curLatestPackageJson is not None:
        print(bcolors.OKGREEN + "Latest Alias found   -- " + curLatestPackageJson.Alias)
        print(bcolors.OKGREEN + "Latest Version found -- " + curLatestPackageJson.Version)
        print(bcolors.OKGREEN + "Latest Subscriber Id -- " + curLatestPackageJson.SubscriberPackageVersionId)
    else:
        print(bcolors.WARNING + "No package Versions Found")

    return curLatestPackageJson

def get_latest_version_subscriber_Id(packageName, DevHubOrg):

    ret = get_latest_package_json(packageName, DevHubOrg)

    if ret is None:
        return None
    else:
        return ret.SubscriberPackageVersionId

def get_latest_version_name(packageName, DevHubOrg):
    return get_latest_package_json(packageName, DevHubOrg).Version

def get_latest_version_alias(packageName, DevHubOrg):
    return get_latest_package_json(packageName, DevHubOrg).Alias



def sfdx_get_next_version_name(packageName):

    # get current latest version number
    versionString = get_latest_version_name(packageName)

    # split the version number on the decimals and incremet the appropriate level
    numList = versionString.split('.')
    numList[len(numList)-1] = str(int(numList[len(numList)-1]) + 1)

    # re splice the integers together to generate the new version number
    nextVersion = '.'.join(numList)

    print(bcolors.OKGREEN + "Next Version number -- " + nextVersion)
    return nextVersion

def sfdx_package_ancestry_tree(packageId, DevHubOrg):
    command = "sfdx force:package:version:displayancestry --package " + packageId + " -v " + DevHubOrg
    displayHumanReadableCommand(command)

def sfdx_package_version_create(packageName, DevHubOrg):

    filename = 'sfdx-project.json'
    if '/automation' in os.getcwd():
        filename = '../' + filename
    if '\\automation' in os.getcwd():
        filename = '../' + filename


    sfdxProjJSON = getJsonFromFile(filename)
    descriptionFORMAT = sfdxProjJSON["packageDirectories"][0]["versionDescription"]
    
    hmisAncestorText = None
    if "ancestorId" in sfdxProjJSON["packageDirectories"][0]:
        hmisAncestorText = sfdxProjJSON["packageDirectories"][0]["ancestorId"]
    elif "ancestorVersion" in sfdxProjJSON["packageDirectories"][0]:
        hmisAncestorText = sfdxProjJSON["packageDirectories"][0]["ancestorVersion"]

    if (hmisAncestorText):
        descriptionFORMAT = descriptionFORMAT.replace('<ANCESTOR_VERSION>', hmisAncestorText)

    if len(sfdxProjJSON["packageDirectories"][0]["dependencies"]) > 0:
        ecmDependencyName = sfdxProjJSON["packageDirectories"][0]["dependencies"][0]["package"]
        descriptionFORMAT = descriptionFORMAT.replace('<ECM_VERSION>', ecmDependencyName)

    if len(sfdxProjJSON["packageDirectories"][0]["dependencies"]) > 1:
        ecmHmisDependencyName = sfdxProjJSON["packageDirectories"][0]["dependencies"][1]["package"]
        descriptionFORMAT = descriptionFORMAT.replace('<ECM_HMIS_VERSION>', ecmHmisDependencyName)

    curDescription = descriptionFORMAT
    print('Version Description: ' + curDescription)

    command = 'sfdx force:package:version:create --package "'+packageName+'" --wait 60 --codecoverage --targetdevhubusername ' + DevHubOrg + ' -x -e "' + curDescription + '"'
    print(bcolors.OKBLUE + 'Running Command: ' + command)
    ret = os.system(command)
    if ret != 0:
        raise Exception(bcolors.FAIL + "Packaging Failed")
    return

def sfdx_unlocked_package_version_create(packageName, DevHubOrg):

    sfdxProjJSON = getJsonFromFile('sfdx-project.json')
    descriptionFORMAT = sfdxProjJSON["packageDirectories"][0]["versionDescription"]
    ecmDependencyName = sfdxProjJSON["packageDirectories"][0]["dependencies"][0]["package"]
    hmisAncestorVersion = sfdxProjJSON["packageDirectories"][0]["ancestorVersion"]
    curDescription = descriptionFORMAT.replace("<ECM_VERSION>", ecmDependencyName).replace('<ANCESTOR_VERSION>', hmisAncestorVersion)
    print('Version Description: ' + curDescription)

    command = 'sfdx force:package:version:create --package "'+packageName+'" --wait 30 --codecoverage --targetdevhubusername ' + DevHubOrg + ' -x -e "' + curDescription + '"'
    print(bcolors.OKBLUE + 'Running Command: ' + command)
    ret = os.system(command)
    if ret != 0:
        raise Exception(bcolors.FAIL + "Packaging Failed")
    return