﻿import os
import subprocess
import json
from collections import namedtuple
from util.command import runCommand, runCommandJSON
from color.bcolors import *




def does_org_alias_exist(orgAlias):
    orgRetList = org_list()
    print('Checking if org '+orgAlias+' exists')
    for i in orgRetList.result.scratchOrgs:
        org = i
        print(org)
        if (hasattr(org, 'alias') and org.alias == orgAlias):
            return True

    return False

def org_list():
    command = 'sfdx force:org:list'
    os.system(command)
    return runCommandJSON(command + ' --json')

def get_org_connection_command(orgAlias):
    sfdxAuthUrl = get_org_SfdxAuthUrl(orgAlias)

    str = 'echo '+sfdxAuthUrl+' > sfdx_auth && sfdx force:auth:sfdxurl:store -f sfdx_auth -d -a "' + orgAlias + '"'

    print(bcolors.WARNING + str)
    return str

def get_org_SfdxAuthUrl(orgAlias):
    jsonRet = org_display_verbose(orgAlias)
    sfdxAuthUrl = jsonRet.result.sfdxAuthUrl

    return sfdxAuthUrl

def org_display_verbose(orgAlias):
    command = 'sfdx force:org:display -u ' + orgAlias + ' --verbose'
    os.system(command)
    return runCommandJSON(command + ' --json')

def org_display(orgAlias, constructLoginUrl):
    command = 'sfdx force:org:display '
    command += '-u ' + orgAlias
    os.system(command)
    ret = runCommandJSON(command + ' --json')

    if (constructLoginUrl == True):
        print(ret.result.instanceUrl + 'secur/frontdoor.jsp?sid=' + ret.result.accessToken)

    return ret

def org_create(scratchOrgAlias, setDefault, devhubOrg):
    command = 'sfdx force:org:create -v ' + devhubOrg + ' -f config/project-scratch-def.json  -w 30 '
    command += ' --setalias ' + scratchOrgAlias
    command += ' --setdefaultusername' if setDefault else ''
    return runCommandJSON(command + ' --json')

def org_create_with_duration(scratchOrgAlias, setDefault, devhubOrg, durationDays):
    command = 'sfdx force:org:create -v ' + devhubOrg + ' -f config/project-scratch-def.json  -w 30 --durationdays ' + str(durationDays)
    command += ' --setalias ' + scratchOrgAlias
    command += ' --setdefaultusername' if setDefault else ''
    return runCommandJSON(command + ' --json')

def org_delete(scratchOrgAliasToDelete, targetdevhubusername):
    command = 'sfdx force:org:delete -p -u ' + scratchOrgAliasToDelete + ' -v ' + targetdevhubusername
    return runCommandJSON(command + ' --json')
