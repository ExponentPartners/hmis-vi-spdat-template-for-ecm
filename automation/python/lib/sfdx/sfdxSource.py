import os
import subprocess
import json
from collections import namedtuple
from util.command import *
from color.bcolors import bcolors

def source_push(orgAlias):
    command = 'sfdx force:source:push -u ' + orgAlias + ' -f'
    print(command)
    runCommand(command)
    return


def source_pull():
    command = 'sfdx force:source:pull'
    print(command)
    ret = runCommandJSON(command)
    print(ret)
    return ret