
import os
import subprocess
import json
from collections import namedtuple

def auth_web_login(DevHubAliasName, loginUrl):
    command = 'sfdx force:auth:web:login -a '+DevHubAliasName+' -r '+loginUrl+''
    print('A window in your Default Browser will open up. Please sign into your ' + DevHubAliasName)
    return json.loads(subprocess.check_output(command + ' --json', shell=True), object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))


