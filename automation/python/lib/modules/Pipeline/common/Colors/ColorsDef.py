
def HEADER():
    return '\033[95m'

def OKBLUE():
    return '\033[94m'

def OKGREEN():
    return '\033[92m'

def WARNING():
    return '\033[93m'

def FAIL():
    return '\033[91m'

def ENDC():
    return '\033[0m'

def BOLD():
    return '\033[1m'

def UNDERLINE():
    return '\033[4m'