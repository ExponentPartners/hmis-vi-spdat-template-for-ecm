
from ..Common.Command import CommandDef
from ..Common.Mod1 import Mod1Def
from ..Common.Mod2 import Mod2Def


def debug():
    Mod1Def.fileFunc1A()
    Mod1Def.fileFunc1B()
    Mod2Def.fileFunc2A()
    Mod2Def.fileFunc2B()
    CommandDef.runCommandJSON('sfdx force:org:list')